namespace EthanolCalculator
{
    public partial class ethanol_calculator : Form
    {
        private List<Melange> _melanges;

        public ethanol_calculator()
        {
            InitializeComponent();
            _melanges = new List<Melange>();
        }

        private void calculer_button_Click(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            try
            {
                for(int i = 0; i < 3; i++)
                {
                    _melanges.Add(new Melange(double.Parse(volume_textBox.Text), double.Parse(part_ethanol_textBox.Text), i));
                }

                if (E10_textBox.Text != String.Empty && E85_textBox.Text != String.Empty)
                {
                    volume_E85_melange_E85_E10_textBox.Text = String.Format("{0:0.####}", _melanges[0].getVolumeE85()).ToString();
                    volume_E10_melange_E85_E10_textBox.Text = String.Format("{0:0.####}", _melanges[0].getVolumeDiluant()).ToString();
                    prix_plein_melange_E85_E10_textBox.Text = String.Format("{0:0.####}", _melanges[0].getPrixPlein(double.Parse(E85_textBox.Text), double.Parse(E10_textBox.Text))).ToString();
                    prix_litre_melange_E85_E10_textBox.Text = String.Format("{0:0.####}", _melanges[0].getPrixLitre(double.Parse(E85_textBox.Text), double.Parse(E10_textBox.Text))).ToString();
                }
                else
                {
                    volume_E85_melange_E85_E10_textBox.Text = String.Empty;
                    volume_E10_melange_E85_E10_textBox.Text = String.Empty;
                    prix_plein_melange_E85_E10_textBox.Text = String.Empty;
                    prix_litre_melange_E85_E10_textBox.Text = String.Empty;
                }
                if (SP95_textBox.Text != String.Empty && E85_textBox.Text != String.Empty)
                {
                    volume_E85_melange_E85_E5_95_textBox.Text = String.Format("{0:0.####}", _melanges[1].getVolumeE85()).ToString();
                    volume_E5_95_melange_E85_E5_95_textBox.Text = String.Format("{0:0.####}", _melanges[1].getVolumeDiluant()).ToString();
                    prix_plein_melange_E85_E5_95_textBox.Text = String.Format("{0:0.####}", _melanges[1].getPrixPlein(double.Parse(E85_textBox.Text), double.Parse(SP95_textBox.Text))).ToString();
                    prix_litre_melange_E85_E5_95_textBox.Text = String.Format("{0:0.####}", _melanges[1].getPrixLitre(double.Parse(E85_textBox.Text), double.Parse(SP95_textBox.Text))).ToString();
                }
                else
                {
                    volume_E85_melange_E85_E5_95_textBox.Text = String.Empty;
                    volume_E5_95_melange_E85_E5_95_textBox.Text = String.Empty;
                    prix_plein_melange_E85_E5_95_textBox.Text = String.Empty;
                    prix_litre_melange_E85_E5_95_textBox.Text = String.Empty;
                }
                if (SP98_textBox.Text != String.Empty && E85_textBox.Text != String.Empty)
                {
                    volume_E85_melange_E85_E5_98_textBox.Text = String.Format("{0:0.####}", _melanges[2].getVolumeE85()).ToString();
                    volume_E5_98_melange_E85_E5_98_textBox.Text = String.Format("{0:0.####}", _melanges[2].getVolumeDiluant()).ToString();
                    prix_plein_melange_E85_E5_98_textBox.Text = String.Format("{0:0.####}", _melanges[2].getPrixPlein(double.Parse(E85_textBox.Text), double.Parse(SP98_textBox.Text))).ToString();
                    prix_litre_melange_E85_E5_98_textBox.Text = String.Format("{0:0.####}", _melanges[2].getPrixLitre(double.Parse(E85_textBox.Text), double.Parse(SP98_textBox.Text))).ToString();
                } else
                {
                    volume_E85_melange_E85_E5_98_textBox.Text = String.Empty;
                    volume_E5_98_melange_E85_E5_98_textBox.Text = String.Empty;
                    prix_plein_melange_E85_E5_98_textBox.Text = String.Empty;
                    prix_litre_melange_E85_E5_98_textBox.Text = String.Empty;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}