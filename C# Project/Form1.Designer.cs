namespace EthanolCalculator
{
    partial class ethanol_calculator
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titre_label = new System.Windows.Forms.Label();
            this.prix_carburant_label = new System.Windows.Forms.Label();
            this.E85_label = new System.Windows.Forms.Label();
            this.SP95_label = new System.Windows.Forms.Label();
            this.SP98_label = new System.Windows.Forms.Label();
            this.E10_label = new System.Windows.Forms.Label();
            this.E85_textBox = new System.Windows.Forms.TextBox();
            this.E10_textBox = new System.Windows.Forms.TextBox();
            this.SP95_textBox = new System.Windows.Forms.TextBox();
            this.SP98_textBox = new System.Windows.Forms.TextBox();
            this.volume_label = new System.Windows.Forms.Label();
            this.volume_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.part_ethanol_textBox = new System.Windows.Forms.TextBox();
            this.resultats_label = new System.Windows.Forms.Label();
            this.melanges_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.possibles_label = new System.Windows.Forms.Label();
            this.volume_E85_melange_E85_E10_textBox = new System.Windows.Forms.TextBox();
            this.volume_E85_melange_E85_E5_95_textBox = new System.Windows.Forms.TextBox();
            this.volume_E85_melange_E85_E5_98_textBox = new System.Windows.Forms.TextBox();
            this.volume2_label = new System.Windows.Forms.Label();
            this.E852_label = new System.Windows.Forms.Label();
            this.volume_E10_melange_E85_E10_textBox = new System.Windows.Forms.TextBox();
            this.volume_E5_95_melange_E85_E5_95_textBox = new System.Windows.Forms.TextBox();
            this.volume_E5_98_melange_E85_E5_98_textBox = new System.Windows.Forms.TextBox();
            this.volume3_label = new System.Windows.Forms.Label();
            this.diluant_label = new System.Windows.Forms.Label();
            this.prix_label = new System.Windows.Forms.Label();
            this.prix_plein_melange_E85_E10_textBox = new System.Windows.Forms.TextBox();
            this.prix_plein_melange_E85_E5_95_textBox = new System.Windows.Forms.TextBox();
            this.prix_plein_melange_E85_E5_98_textBox = new System.Windows.Forms.TextBox();
            this.plein_label = new System.Windows.Forms.Label();
            this.prix2_label = new System.Windows.Forms.Label();
            this.litre_label = new System.Windows.Forms.Label();
            this.prix_litre_melange_E85_E10_textBox = new System.Windows.Forms.TextBox();
            this.prix_litre_melange_E85_E5_95_textBox = new System.Windows.Forms.TextBox();
            this.prix_litre_melange_E85_E5_98_textBox = new System.Windows.Forms.TextBox();
            this.calculer_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titre_label
            // 
            this.titre_label.AutoSize = true;
            this.titre_label.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.titre_label.Location = new System.Drawing.Point(85, 9);
            this.titre_label.Name = "titre_label";
            this.titre_label.Size = new System.Drawing.Size(207, 32);
            this.titre_label.TabIndex = 0;
            this.titre_label.Text = "Ethanol Calculator";
            // 
            // prix_carburant_label
            // 
            this.prix_carburant_label.AutoSize = true;
            this.prix_carburant_label.Location = new System.Drawing.Point(28, 53);
            this.prix_carburant_label.Name = "prix_carburant_label";
            this.prix_carburant_label.Size = new System.Drawing.Size(116, 15);
            this.prix_carburant_label.TabIndex = 1;
            this.prix_carburant_label.Text = "Prix Carburants (€/L)";
            // 
            // E85_label
            // 
            this.E85_label.AutoSize = true;
            this.E85_label.Location = new System.Drawing.Point(28, 79);
            this.E85_label.Name = "E85_label";
            this.E85_label.Size = new System.Drawing.Size(25, 15);
            this.E85_label.TabIndex = 2;
            this.E85_label.Text = "E85";
            // 
            // SP95_label
            // 
            this.SP95_label.AutoSize = true;
            this.SP95_label.Location = new System.Drawing.Point(11, 137);
            this.SP95_label.Name = "SP95_label";
            this.SP95_label.Size = new System.Drawing.Size(42, 15);
            this.SP95_label.TabIndex = 4;
            this.SP95_label.Text = "E5 (95)";
            // 
            // SP98_label
            // 
            this.SP98_label.AutoSize = true;
            this.SP98_label.Location = new System.Drawing.Point(11, 166);
            this.SP98_label.Name = "SP98_label";
            this.SP98_label.Size = new System.Drawing.Size(42, 15);
            this.SP98_label.TabIndex = 6;
            this.SP98_label.Text = "E5 (98)";
            // 
            // E10_label
            // 
            this.E10_label.AutoSize = true;
            this.E10_label.Location = new System.Drawing.Point(28, 108);
            this.E10_label.Name = "E10_label";
            this.E10_label.Size = new System.Drawing.Size(25, 15);
            this.E10_label.TabIndex = 3;
            this.E10_label.Text = "E10";
            // 
            // E85_textBox
            // 
            this.E85_textBox.Location = new System.Drawing.Point(59, 76);
            this.E85_textBox.Name = "E85_textBox";
            this.E85_textBox.Size = new System.Drawing.Size(85, 23);
            this.E85_textBox.TabIndex = 7;
            // 
            // E10_textBox
            // 
            this.E10_textBox.Location = new System.Drawing.Point(59, 105);
            this.E10_textBox.Name = "E10_textBox";
            this.E10_textBox.Size = new System.Drawing.Size(85, 23);
            this.E10_textBox.TabIndex = 8;
            // 
            // SP95_textBox
            // 
            this.SP95_textBox.Location = new System.Drawing.Point(59, 134);
            this.SP95_textBox.Name = "SP95_textBox";
            this.SP95_textBox.Size = new System.Drawing.Size(85, 23);
            this.SP95_textBox.TabIndex = 9;
            // 
            // SP98_textBox
            // 
            this.SP98_textBox.Location = new System.Drawing.Point(59, 163);
            this.SP98_textBox.Name = "SP98_textBox";
            this.SP98_textBox.Size = new System.Drawing.Size(85, 23);
            this.SP98_textBox.TabIndex = 10;
            // 
            // volume_label
            // 
            this.volume_label.AutoSize = true;
            this.volume_label.Location = new System.Drawing.Point(202, 53);
            this.volume_label.Name = "volume_label";
            this.volume_label.Size = new System.Drawing.Size(113, 15);
            this.volume_label.TabIndex = 11;
            this.volume_label.Text = "Volume Souhaité (L)";
            // 
            // volume_textBox
            // 
            this.volume_textBox.Location = new System.Drawing.Point(202, 76);
            this.volume_textBox.Name = "volume_textBox";
            this.volume_textBox.Size = new System.Drawing.Size(100, 23);
            this.volume_textBox.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(202, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "Part d\'Ethanol Souhaitée (%)";
            // 
            // part_ethanol_textBox
            // 
            this.part_ethanol_textBox.Location = new System.Drawing.Point(202, 134);
            this.part_ethanol_textBox.Name = "part_ethanol_textBox";
            this.part_ethanol_textBox.Size = new System.Drawing.Size(100, 23);
            this.part_ethanol_textBox.TabIndex = 14;
            // 
            // resultats_label
            // 
            this.resultats_label.AutoSize = true;
            this.resultats_label.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.resultats_label.Location = new System.Drawing.Point(126, 202);
            this.resultats_label.Name = "resultats_label";
            this.resultats_label.Size = new System.Drawing.Size(108, 32);
            this.resultats_label.TabIndex = 15;
            this.resultats_label.Text = "Résultats";
            // 
            // melanges_label
            // 
            this.melanges_label.AutoSize = true;
            this.melanges_label.Location = new System.Drawing.Point(11, 246);
            this.melanges_label.Name = "melanges_label";
            this.melanges_label.Size = new System.Drawing.Size(58, 15);
            this.melanges_label.TabIndex = 16;
            this.melanges_label.Text = "Mélanges";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 15);
            this.label2.TabIndex = 17;
            this.label2.Text = "E85 / E10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 314);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "E85 / E5 (95)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 343);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "E85 / E5 (98)";
            // 
            // possibles_label
            // 
            this.possibles_label.AutoSize = true;
            this.possibles_label.Location = new System.Drawing.Point(11, 261);
            this.possibles_label.Name = "possibles_label";
            this.possibles_label.Size = new System.Drawing.Size(55, 15);
            this.possibles_label.TabIndex = 20;
            this.possibles_label.Text = "Possibles";
            // 
            // volume_E85_melange_E85_E10_textBox
            // 
            this.volume_E85_melange_E85_E10_textBox.Location = new System.Drawing.Point(89, 282);
            this.volume_E85_melange_E85_E10_textBox.Name = "volume_E85_melange_E85_E10_textBox";
            this.volume_E85_melange_E85_E10_textBox.ReadOnly = true;
            this.volume_E85_melange_E85_E10_textBox.Size = new System.Drawing.Size(55, 23);
            this.volume_E85_melange_E85_E10_textBox.TabIndex = 21;
            // 
            // volume_E85_melange_E85_E5_95_textBox
            // 
            this.volume_E85_melange_E85_E5_95_textBox.Location = new System.Drawing.Point(89, 311);
            this.volume_E85_melange_E85_E5_95_textBox.Name = "volume_E85_melange_E85_E5_95_textBox";
            this.volume_E85_melange_E85_E5_95_textBox.ReadOnly = true;
            this.volume_E85_melange_E85_E5_95_textBox.Size = new System.Drawing.Size(55, 23);
            this.volume_E85_melange_E85_E5_95_textBox.TabIndex = 22;
            // 
            // volume_E85_melange_E85_E5_98_textBox
            // 
            this.volume_E85_melange_E85_E5_98_textBox.Location = new System.Drawing.Point(89, 340);
            this.volume_E85_melange_E85_E5_98_textBox.Name = "volume_E85_melange_E85_E5_98_textBox";
            this.volume_E85_melange_E85_E5_98_textBox.ReadOnly = true;
            this.volume_E85_melange_E85_E5_98_textBox.Size = new System.Drawing.Size(55, 23);
            this.volume_E85_melange_E85_E5_98_textBox.TabIndex = 23;
            // 
            // volume2_label
            // 
            this.volume2_label.AutoSize = true;
            this.volume2_label.Location = new System.Drawing.Point(86, 246);
            this.volume2_label.Name = "volume2_label";
            this.volume2_label.Size = new System.Drawing.Size(47, 15);
            this.volume2_label.TabIndex = 24;
            this.volume2_label.Text = "Volume";
            // 
            // E852_label
            // 
            this.E852_label.AutoSize = true;
            this.E852_label.Location = new System.Drawing.Point(89, 261);
            this.E852_label.Name = "E852_label";
            this.E852_label.Size = new System.Drawing.Size(25, 15);
            this.E852_label.TabIndex = 25;
            this.E852_label.Text = "E85";
            // 
            // volume_E10_melange_E85_E10_textBox
            // 
            this.volume_E10_melange_E85_E10_textBox.Location = new System.Drawing.Point(150, 282);
            this.volume_E10_melange_E85_E10_textBox.Name = "volume_E10_melange_E85_E10_textBox";
            this.volume_E10_melange_E85_E10_textBox.ReadOnly = true;
            this.volume_E10_melange_E85_E10_textBox.Size = new System.Drawing.Size(55, 23);
            this.volume_E10_melange_E85_E10_textBox.TabIndex = 26;
            // 
            // volume_E5_95_melange_E85_E5_95_textBox
            // 
            this.volume_E5_95_melange_E85_E5_95_textBox.Location = new System.Drawing.Point(150, 311);
            this.volume_E5_95_melange_E85_E5_95_textBox.Name = "volume_E5_95_melange_E85_E5_95_textBox";
            this.volume_E5_95_melange_E85_E5_95_textBox.ReadOnly = true;
            this.volume_E5_95_melange_E85_E5_95_textBox.Size = new System.Drawing.Size(55, 23);
            this.volume_E5_95_melange_E85_E5_95_textBox.TabIndex = 27;
            // 
            // volume_E5_98_melange_E85_E5_98_textBox
            // 
            this.volume_E5_98_melange_E85_E5_98_textBox.Location = new System.Drawing.Point(150, 340);
            this.volume_E5_98_melange_E85_E5_98_textBox.Name = "volume_E5_98_melange_E85_E5_98_textBox";
            this.volume_E5_98_melange_E85_E5_98_textBox.ReadOnly = true;
            this.volume_E5_98_melange_E85_E5_98_textBox.Size = new System.Drawing.Size(55, 23);
            this.volume_E5_98_melange_E85_E5_98_textBox.TabIndex = 28;
            // 
            // volume3_label
            // 
            this.volume3_label.AutoSize = true;
            this.volume3_label.Location = new System.Drawing.Point(150, 246);
            this.volume3_label.Name = "volume3_label";
            this.volume3_label.Size = new System.Drawing.Size(47, 15);
            this.volume3_label.TabIndex = 29;
            this.volume3_label.Text = "Volume";
            // 
            // diluant_label
            // 
            this.diluant_label.AutoSize = true;
            this.diluant_label.Location = new System.Drawing.Point(150, 261);
            this.diluant_label.Name = "diluant_label";
            this.diluant_label.Size = new System.Drawing.Size(45, 15);
            this.diluant_label.TabIndex = 30;
            this.diluant_label.Text = "Diluant";
            // 
            // prix_label
            // 
            this.prix_label.AutoSize = true;
            this.prix_label.Location = new System.Drawing.Point(214, 246);
            this.prix_label.Name = "prix_label";
            this.prix_label.Size = new System.Drawing.Size(44, 15);
            this.prix_label.TabIndex = 31;
            this.prix_label.Text = "Prix du";
            // 
            // prix_plein_melange_E85_E10_textBox
            // 
            this.prix_plein_melange_E85_E10_textBox.Location = new System.Drawing.Point(211, 282);
            this.prix_plein_melange_E85_E10_textBox.Name = "prix_plein_melange_E85_E10_textBox";
            this.prix_plein_melange_E85_E10_textBox.ReadOnly = true;
            this.prix_plein_melange_E85_E10_textBox.Size = new System.Drawing.Size(55, 23);
            this.prix_plein_melange_E85_E10_textBox.TabIndex = 32;
            // 
            // prix_plein_melange_E85_E5_95_textBox
            // 
            this.prix_plein_melange_E85_E5_95_textBox.Location = new System.Drawing.Point(211, 311);
            this.prix_plein_melange_E85_E5_95_textBox.Name = "prix_plein_melange_E85_E5_95_textBox";
            this.prix_plein_melange_E85_E5_95_textBox.ReadOnly = true;
            this.prix_plein_melange_E85_E5_95_textBox.Size = new System.Drawing.Size(55, 23);
            this.prix_plein_melange_E85_E5_95_textBox.TabIndex = 33;
            // 
            // prix_plein_melange_E85_E5_98_textBox
            // 
            this.prix_plein_melange_E85_E5_98_textBox.Location = new System.Drawing.Point(211, 340);
            this.prix_plein_melange_E85_E5_98_textBox.Name = "prix_plein_melange_E85_E5_98_textBox";
            this.prix_plein_melange_E85_E5_98_textBox.ReadOnly = true;
            this.prix_plein_melange_E85_E5_98_textBox.Size = new System.Drawing.Size(55, 23);
            this.prix_plein_melange_E85_E5_98_textBox.TabIndex = 34;
            // 
            // plein_label
            // 
            this.plein_label.AutoSize = true;
            this.plein_label.Location = new System.Drawing.Point(214, 261);
            this.plein_label.Name = "plein_label";
            this.plein_label.Size = new System.Drawing.Size(50, 15);
            this.plein_label.TabIndex = 35;
            this.plein_label.Text = "Plein (€)";
            // 
            // prix2_label
            // 
            this.prix2_label.AutoSize = true;
            this.prix2_label.Location = new System.Drawing.Point(271, 246);
            this.prix2_label.Name = "prix2_label";
            this.prix2_label.Size = new System.Drawing.Size(43, 15);
            this.prix2_label.TabIndex = 36;
            this.prix2_label.Text = "Prix au";
            // 
            // litre_label
            // 
            this.litre_label.AutoSize = true;
            this.litre_label.Location = new System.Drawing.Point(271, 261);
            this.litre_label.Name = "litre_label";
            this.litre_label.Size = new System.Drawing.Size(58, 15);
            this.litre_label.TabIndex = 37;
            this.litre_label.Text = "Litre (€/L)";
            // 
            // prix_litre_melange_E85_E10_textBox
            // 
            this.prix_litre_melange_E85_E10_textBox.Location = new System.Drawing.Point(272, 282);
            this.prix_litre_melange_E85_E10_textBox.Name = "prix_litre_melange_E85_E10_textBox";
            this.prix_litre_melange_E85_E10_textBox.ReadOnly = true;
            this.prix_litre_melange_E85_E10_textBox.Size = new System.Drawing.Size(55, 23);
            this.prix_litre_melange_E85_E10_textBox.TabIndex = 38;
            // 
            // prix_litre_melange_E85_E5_95_textBox
            // 
            this.prix_litre_melange_E85_E5_95_textBox.Location = new System.Drawing.Point(272, 311);
            this.prix_litre_melange_E85_E5_95_textBox.Name = "prix_litre_melange_E85_E5_95_textBox";
            this.prix_litre_melange_E85_E5_95_textBox.ReadOnly = true;
            this.prix_litre_melange_E85_E5_95_textBox.Size = new System.Drawing.Size(55, 23);
            this.prix_litre_melange_E85_E5_95_textBox.TabIndex = 39;
            // 
            // prix_litre_melange_E85_E5_98_textBox
            // 
            this.prix_litre_melange_E85_E5_98_textBox.Location = new System.Drawing.Point(272, 340);
            this.prix_litre_melange_E85_E5_98_textBox.Name = "prix_litre_melange_E85_E5_98_textBox";
            this.prix_litre_melange_E85_E5_98_textBox.ReadOnly = true;
            this.prix_litre_melange_E85_E5_98_textBox.Size = new System.Drawing.Size(55, 23);
            this.prix_litre_melange_E85_E5_98_textBox.TabIndex = 40;
            // 
            // calculer_button
            // 
            this.calculer_button.Location = new System.Drawing.Point(202, 166);
            this.calculer_button.Name = "calculer_button";
            this.calculer_button.Size = new System.Drawing.Size(76, 23);
            this.calculer_button.TabIndex = 41;
            this.calculer_button.Text = "Calculer";
            this.calculer_button.UseVisualStyleBackColor = true;
            this.calculer_button.Click += new System.EventHandler(this.calculer_button_Click);
            // 
            // ethanol_calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 383);
            this.Controls.Add(this.calculer_button);
            this.Controls.Add(this.prix_litre_melange_E85_E5_98_textBox);
            this.Controls.Add(this.prix_litre_melange_E85_E5_95_textBox);
            this.Controls.Add(this.prix_litre_melange_E85_E10_textBox);
            this.Controls.Add(this.litre_label);
            this.Controls.Add(this.prix2_label);
            this.Controls.Add(this.plein_label);
            this.Controls.Add(this.prix_plein_melange_E85_E5_98_textBox);
            this.Controls.Add(this.prix_plein_melange_E85_E5_95_textBox);
            this.Controls.Add(this.prix_plein_melange_E85_E10_textBox);
            this.Controls.Add(this.prix_label);
            this.Controls.Add(this.diluant_label);
            this.Controls.Add(this.volume3_label);
            this.Controls.Add(this.volume_E5_98_melange_E85_E5_98_textBox);
            this.Controls.Add(this.volume_E5_95_melange_E85_E5_95_textBox);
            this.Controls.Add(this.volume_E10_melange_E85_E10_textBox);
            this.Controls.Add(this.E852_label);
            this.Controls.Add(this.volume2_label);
            this.Controls.Add(this.volume_E85_melange_E85_E5_98_textBox);
            this.Controls.Add(this.volume_E85_melange_E85_E5_95_textBox);
            this.Controls.Add(this.volume_E85_melange_E85_E10_textBox);
            this.Controls.Add(this.possibles_label);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.melanges_label);
            this.Controls.Add(this.resultats_label);
            this.Controls.Add(this.part_ethanol_textBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.volume_textBox);
            this.Controls.Add(this.volume_label);
            this.Controls.Add(this.SP98_textBox);
            this.Controls.Add(this.SP95_textBox);
            this.Controls.Add(this.E10_textBox);
            this.Controls.Add(this.E85_textBox);
            this.Controls.Add(this.SP98_label);
            this.Controls.Add(this.SP95_label);
            this.Controls.Add(this.E10_label);
            this.Controls.Add(this.E85_label);
            this.Controls.Add(this.prix_carburant_label);
            this.Controls.Add(this.titre_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ethanol_calculator";
            this.Text = "Ethanol Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label titre_label;
        private Label prix_carburant_label;
        private Label E85_label;
        private Label SP95_label;
        private Label SP98_label;
        private Label E10_label;
        private TextBox E85_textBox;
        private TextBox E10_textBox;
        private TextBox SP95_textBox;
        private TextBox SP98_textBox;
        private Label volume_label;
        private TextBox volume_textBox;
        private Label label1;
        private TextBox part_ethanol_textBox;
        private Label resultats_label;
        private Label melanges_label;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label possibles_label;
        private TextBox volume_E85_melange_E85_E10_textBox;
        private TextBox volume_E85_melange_E85_E5_95_textBox;
        private TextBox volume_E85_melange_E85_E5_98_textBox;
        private Label volume2_label;
        private Label E852_label;
        private TextBox volume_E10_melange_E85_E10_textBox;
        private TextBox volume_E5_95_melange_E85_E5_95_textBox;
        private TextBox volume_E5_98_melange_E85_E5_98_textBox;
        private Label volume3_label;
        private Label diluant_label;
        private Label prix_label;
        private TextBox prix_plein_melange_E85_E10_textBox;
        private TextBox prix_plein_melange_E85_E5_95_textBox;
        private TextBox prix_plein_melange_E85_E5_98_textBox;
        private Label plein_label;
        private Label prix2_label;
        private Label litre_label;
        private TextBox prix_litre_melange_E85_E10_textBox;
        private TextBox prix_litre_melange_E85_E5_95_textBox;
        private TextBox prix_litre_melange_E85_E5_98_textBox;
        private Button calculer_button;
    }
}