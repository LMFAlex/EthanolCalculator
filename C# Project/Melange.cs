using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EthanolCalculator
{
    internal class Melange
    {
        private double _volume;
        private double _partEthanol;
        private int _partEthanolDiluant;

        public Melange(double vol, double part, int diluant)
        {
            _volume = vol;
            _partEthanol = part;
            switch (diluant)
            {
                case 0:
                    _partEthanolDiluant = 10;
                    break;
                case 1:
                    _partEthanolDiluant = 5;
                    break;
                case 2:
                    _partEthanolDiluant = 5;
                    break;
                default:
                    MessageBox.Show("Attention ! Le paramètre diluant doit valoir 0, 1 ou 2 !");
                    break;
            }
        }

        public double getVolume()
        {
            return _volume;
        }

        public double getPartEthanol()
        {
            return _partEthanol;
        }

        public double getVolumeE85()
        {
            return (((_partEthanol - _partEthanolDiluant) / (85 - _partEthanolDiluant)) * _volume);
        }

        public double getVolumeDiluant()
        {
            return (_volume -  getVolumeE85());
        }
        
        public double getPrixPlein(double prixE85 = 0, double prixDiluant = 0)
        {
            return (prixE85 * getVolumeE85() + prixDiluant * getVolumeDiluant());
        }

        public double getPrixLitre(double prixE85 = 0, double prixDiluant = 0)
        {
            return (getPrixPlein(prixE85, prixDiluant) / _volume);
        }
    }
}
